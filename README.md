# vlakbij

Vlakbij ("nearby" in Dutch) is an Android application that uses the Foursquare API to show nearby venues. The app is written in Kotlin and uses RxJava to handle asynchronous calls.

## Building locally

A Foursquare API _Client ID_ and _Client Secret_ are required to build and run this app. These can be obtained [here](https://developer.foursquare.com/docs/api).

Once you have your client id and client secret, you will need to update [FoursquareClientProvider](https://gitlab.com/brigit.lyons/vlakbij/blob/master/app/src/main/java/com/brigitlyons/vlakbij/api/FoursquareClientProvider.kt#L61) to use them.

### Running on an Android emulator

You should use an Android emulator that has Google Play Services support. The Android emulator may not initialy have a location available. You can activate the location by opening the Google Maps app and requesting the current position. You can modify the emulator's position by opening the location settings and adjusting the latitude and longitude values.

## Project specifics

### Location
The user's location is determined using the [Location API](https://developer.android.com/training/location/). This requires that the user grants the `location` permission to the app. The initial screen handles requesting this permission, as well as handling error cases when:

* the user has initially denied the request (action: request permission again)
* the user has denied with the "do not ask again" flag (action: open app settings to enable permission manually)
* when requesting the location, the request is canceled or an error occurs (no action)

### Foursquare API
The app uses Foursquare's [/venues/explore](https://developer.foursquare.com/docs/api/venues/explore) and [/venues/\<venue_id\>/photos](https://developer.foursquare.com/docs/api/venues/photos) endpoints to fetch recommended venues, as well a photo of the venue. The app initially requests a list of venue recommendations. For every venue in the result list, the app then fetches a photo. This means that, when the app receives a response with `n` venues, it will make a total of `n+1` network requests.

#### Limitation

The `n` network requests for the venue photos are initiated in parallel, however, using the Personal tier of the Foursquare API limits the app to 2 QPS (queries per second). This limitation can cause the loading of the venues to be slow in cases where there are many venues in the response.

#### Potential future workarounds:

* upgrade to a higher API tier without QPS limitations
* limit the number of venues that are requested per response (would likely also require adding an option for user to load more)
* cache network responses, especially venue photo urls

### Android Architecture Components

Several of the Android Architecture Components were used to create this app, including:

* [LiveData](https://developer.android.com/topic/libraries/architecture/livedata)
* [Data Binding](https://developer.android.com/topic/libraries/data-binding/)
* [ViewModels](https://developer.android.com/topic/libraries/architecture/viewmodel)
* [Navigation](https://developer.android.com/topic/libraries/architecture/navigation/)
 
### Other libraries

Some other libraries have also been used to create this app, including:

* [RxJava](https://github.com/ReactiveX/RxJava)
* [RxAndroid](https://github.com/ReactiveX/RxAndroid)
* [Stetho](https://github.com/facebook/stetho)
* [Retrofit](https://square.github.io/retrofit/)
* [OkHttp3](https://square.github.io/okhttp/3.x/okhttp/) (with Stetho and Retrofit)
* [Glide](https://github.com/bumptech/glide)
* [gson](https://github.com/google/gson)
 
### Dependency Injection

A very light-weight approach to dependency injection has been taken, with a manually configured [DependencyInjector](https://gitlab.com/brigit.lyons/vlakbij/blob/master/app/src/main/java/com/brigitlyons/vlakbij/util/DependencyInjector.kt) class. This is still quite managable for a small project, but this could be updated in the future to use something like [Dagger](https://github.com/google/dagger) or [Koin](https://github.com/InsertKoinIO/koin)

### Testing

The app is not completely tested due to time contstaints. However, the most crucial/complex components have been tested to give an example of best practices:

* [FoursquareRepositoryTest](https://gitlab.com/brigit.lyons/vlakbij/blob/master/app/src/test/java/com/brigitlyons/vlakbij/api/FoursquareRepositoryTest.kt)
* [VenuesViewModelTest](https://gitlab.com/brigit.lyons/vlakbij/blob/master/app/src/test/java/com/brigitlyons/vlakbij/viewmodel/VenuesViewModelTest.kt)

Unit tests are preferred for faster execution and ability to run without Android environment. Some testing libraries used include:

* [mockito-kotlin](https://github.com/nhaarman/mockito-kotlin)
* [Truth](https://github.com/google/truth)

## Future improvements

Several improvements could be made to extend the functionality and usability of the app. Aside from the caching of network responses that has already been mentioned, some ideas include:

* Add additional filtering options for searches:
  * search by location string instead of latitude/longitude
  * include a search radius
  * filter by venues that are open now
  * filter venues by price rating
  * sort venues by proximity
* Integrating the [Google Maps SDK](https://developers.google.com/maps/documentation/android-sdk/intro):
  * allow users to see their location on a map, and add gesture control to change their search location or radius
  * allow users to see venue recommendation on a map, in relation to the search location
* Allow users to click on a venue card to view more in-depth information about the venue
* Allow users to save a venue to a list of their favorites
* Allow users to browse a list of their favorite venues
* Highlight and include favorite venues in search results
* Introduce paging when making/reviewing search results (maybe with [Paging library](https://developer.android.com/topic/libraries/architecture/paging/))
* Add a recovery action for when requesting the user's location fails

