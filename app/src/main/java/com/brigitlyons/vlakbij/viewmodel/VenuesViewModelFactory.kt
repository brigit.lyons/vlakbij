package com.brigitlyons.vlakbij.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.brigitlyons.vlakbij.api.FoursquareRepository
import com.brigitlyons.vlakbij.view.VenuesFragmentParams

class VenuesViewModelFactory(
        private val repository: FoursquareRepository,
        private val venuesFragmentParams: VenuesFragmentParams,
        private val view: VenuesViewModel.View
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
            VenuesViewModel(repository, venuesFragmentParams, view) as T
}