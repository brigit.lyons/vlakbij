package com.brigitlyons.vlakbij.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.brigitlyons.vlakbij.view.OptionsFragmentParams

class OptionsViewModelFactory(
        private val optionsFragmentParams: OptionsFragmentParams,
        private val view: OptionsViewModel.View
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
            OptionsViewModel(optionsFragmentParams, view) as T
}