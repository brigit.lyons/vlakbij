package com.brigitlyons.vlakbij.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.location.Location
import android.view.View.OnClickListener
import androidx.navigation.Navigation
import com.brigitlyons.vlakbij.R
import com.brigitlyons.vlakbij.view.OptionsFragmentParams
import com.brigitlyons.vlakbij.view.VenuesFragmentParams

class OptionsViewModel(
        private val params: OptionsFragmentParams,
        private val view: View
) : ViewModel() {
    private val query = MutableLiveData<String>()

    fun getLocationText(): String = "${getLocation().latitude.round()}, ${getLocation().longitude.round()}"

    private fun getLocation(): Location = params.location

    fun showVenuesButtonClick(): OnClickListener =
            OnClickListener {
                view.closeKeyboard()
                Navigation.findNavController(it).navigate(R.id.action_optionsFragment_to_venuesFragment, VenuesFragmentParams.toBundle(getLocation(), query.value))
            }

    fun getQuery() = query

    private fun Double.round() = "%.1f".format(this)

    interface View {
        fun closeKeyboard()
    }
}