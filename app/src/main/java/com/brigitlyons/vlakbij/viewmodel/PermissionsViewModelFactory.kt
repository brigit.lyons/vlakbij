package com.brigitlyons.vlakbij.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider

class PermissionsViewModelFactory(
        private val view: PermissionsViewModel.View
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
            PermissionsViewModel(view) as T
}