package com.brigitlyons.vlakbij.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.brigitlyons.vlakbij.api.FoursquareRepository
import com.brigitlyons.vlakbij.ui.Venue
import com.brigitlyons.vlakbij.view.VenuesFragmentParams
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import android.view.View.OnClickListener
import com.brigitlyons.vlakbij.api.EmptyVenuesResponse
import com.brigitlyons.vlakbij.api.SuccessVenuesResponse
import com.brigitlyons.vlakbij.api.VenuesResponse
import io.reactivex.Scheduler

class VenuesViewModel(
        private val foursquareRepository: FoursquareRepository,
        private val venuesFragmentParams: VenuesFragmentParams,
        private val view: View,
        private val subscribeScheduler: Scheduler = Schedulers.io(),
        private val observeScheduler: Scheduler = AndroidSchedulers.mainThread()
) : ViewModel() {
    private val inProgress = MutableLiveData<Boolean>()
    private val error = MutableLiveData<Boolean>()
    private val empty = MutableLiveData<Boolean>()
    private val errorHeader = MutableLiveData<String>()
    private val errorSubText = MutableLiveData<String>()
    private val errorButtonText = MutableLiveData<String>()
    private val venues = MutableLiveData<List<Venue>>()
    private lateinit var disposable: Disposable

    init {
        requestVenuesAndSetValues()
    }

    fun onDestroy() = disposable.dispose()

    fun getVenues() = venues

    fun getInProgress() = inProgress

    fun getError() = error

    fun getEmpty() = empty

    fun getErrorHeader() = errorHeader

    fun getErrorSubText() = errorSubText

    fun getErrorButtonText() = errorButtonText

    fun handleErrorButtonClick() = OnClickListener {
        if (empty.value == true) {
            view.goBack()
        } else if (error.value == true) {
            requestVenuesAndSetValues()
        }
    }

    private fun requestVenuesAndSetValues() {
        inProgress.value = true
        error.value = false
        empty.value = false
        setErrorStrings(null, null, null)

        requestVenues()
    }

    private fun requestVenues() {
        val location = venuesFragmentParams.location
        disposable = foursquareRepository.getVenueRecommendations(location.latitude, location.longitude, venuesFragmentParams.query)
                .subscribeOn(subscribeScheduler)
                .observeOn(observeScheduler)
                .subscribe(
                        { handleSuccessResponse(it) },
                        { handleFailureResponse(it) }
                )
    }

    private fun handleSuccessResponse(response: VenuesResponse) {
        error.value = false
        inProgress.value = false
        when (response) {
            is EmptyVenuesResponse -> setEmptyState(response.warning)
            is SuccessVenuesResponse -> {
                venues.value = response.venues
                if (response.venues.isEmpty()) {
                    setEmptyState(subText = null)
                }
            }
        }
    }

    private fun handleFailureResponse(throwable: Throwable) {
        error.value = true
        empty.value = false
        inProgress.value = false
        setErrorStrings(
                view.errorHeader(),
                view.errorSubText(),
                view.errorButtonText())
        throwable.printStackTrace()
    }

    private fun setEmptyState(subText: String?) {
        empty.value = true
        setErrorStrings(
                view.emptyHeader(),
                subText,
                view.emptyButtonText())
    }

    private fun setErrorStrings(header: String?, subText: String?, button: String?) {
        errorHeader.value = header
        errorSubText.value = subText
        errorButtonText.value = button
    }

    interface View {
        fun goBack()
        fun errorHeader(): String
        fun errorSubText(): String
        fun errorButtonText(): String
        fun emptyHeader(): String
        fun emptyButtonText(): String
    }
}
