package com.brigitlyons.vlakbij.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.location.Location
import com.brigitlyons.vlakbij.util.PermissionManager

class PermissionsViewModel(private val view: View) : ViewModel() {
    private val inProgress = MutableLiveData<Boolean>()
    private val location = MutableLiveData<Location>()
    private val permissionState = MutableLiveData<PermissionManager.State>()
    private val header = MutableLiveData<String>()
    private val subText = MutableLiveData<String>()
    private val buttonText = MutableLiveData<String>()

    init {
        inProgress.value = true
    }

    fun getInProgress() = inProgress

    fun getHeader() = header

    fun getSubText() = subText

    fun getButtonText() = buttonText

    fun setLocation(location: Location) {
        this.location.postValue(location)
    }

    fun handleButtonClick() {
        when (permissionState.value) {
            PermissionManager.State.REQUEST -> view.requestPermission()
            PermissionManager.State.DENIED -> view.openAppSettings()
        }
    }

    fun setPermissionState(state: PermissionManager.State) {
        permissionState.value = state
        when (state) {
            PermissionManager.State.GRANTED -> view.getLocation()
            PermissionManager.State.REQUEST -> setRequestPermissionState()
            PermissionManager.State.DENIED -> setChangePermissionInSettingsState()
        }
    }

    fun setErrorState() {
        inProgress.value = false
        header.value = view.getErrorHeader()
        subText.value = view.getErrorSubText()
        buttonText.value = null
    }

    private fun setRequestPermissionState() {
        setPermissionsInfoState()
        buttonText.value = view.getPermissionsRequestButtonText()
    }

    private fun setChangePermissionInSettingsState() {
        setPermissionsInfoState()
        buttonText.value = view.getPermissionsSettingsButtonText()
    }

    private fun setPermissionsInfoState() {
        inProgress.value = false
        header.value = view.getPermissionsHeader()
        subText.value = view.getPermissionsSubText()
    }

    interface View {
        fun requestPermission()
        fun openAppSettings()
        fun getLocation()
        fun getErrorHeader(): String
        fun getErrorSubText(): String
        fun getPermissionsHeader(): String
        fun getPermissionsSubText(): String
        fun getPermissionsRequestButtonText(): String
        fun getPermissionsSettingsButtonText(): String
    }
}
