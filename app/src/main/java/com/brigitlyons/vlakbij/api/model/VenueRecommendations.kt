package com.brigitlyons.vlakbij.api.model

import com.google.gson.annotations.SerializedName

class VenueRecommendations {
    data class Response(
            @SerializedName("response") val responseItem: ResponseItem
    )

    data class ResponseItem(
            @SerializedName("groups") val groups: List<Group>,
            @SerializedName("warning") val warning: Warning?,
            @SerializedName("totalResults") val totalResults: Int
    )

    data class Warning(
            @SerializedName("text") val text: String
    )

    data class Group(
            @SerializedName("items") val items: List<GroupItem>
    )

    data class GroupItem(
            @SerializedName("venue") val venue: Venue
    )

    data class Venue(
            @SerializedName("name") val name: String,
            @SerializedName("id") val id: String,
            @SerializedName("location") val location: Location
    )

    data class Location(
            @SerializedName("formattedAddress") val address: List<String>
    )
}