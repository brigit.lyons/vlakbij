package com.brigitlyons.vlakbij.api.model

import com.google.gson.annotations.SerializedName

class VenuePhotos {
    data class Response(
            @SerializedName("response") val photos: Photos
    )

    data class Photos(
            @SerializedName("photos") val photo: Photo
    )

    data class Photo(
            @SerializedName("items") val items: List<PhotoItem>
    )

    data class PhotoItem(
            @SerializedName("prefix") val prefix: String,
            @SerializedName("suffix") val suffix: String
    )
}