package com.brigitlyons.vlakbij.api

import com.brigitlyons.vlakbij.api.model.VenueRecommendations
import com.brigitlyons.vlakbij.ui.Venue
import io.reactivex.Single

open class FoursquareRepository(private val foursquareClient: FoursquareClient) {

    open fun getVenueRecommendations(latitude: Double, longitude: Double, query: String?): Single<VenuesResponse> =
            getVenueRecommendationsWithoutPhoto(latitude, longitude, query)
                    .flatMap { response ->
                        when (response) {
                            is EmptyRecommendationsResponse -> Single.just(EmptyVenuesResponse(response.warning))
                            is SuccessRecommendationsResponse -> mapToSuccessVenuesResponse(response)
                        }
                    }

    private fun getVenueRecommendationsWithoutPhoto(latitude: Double, longitude: Double, query: String?): Single<RecommendationsResponse> =
            foursquareClient.getVenueRecommendations("$latitude,$longitude", query)
                    .map { response ->
                        if (response.responseItem.totalResults == 0) {
                            EmptyRecommendationsResponse(response.responseItem.warning?.text)
                        } else {
                            mapToSuccessRecommendationsResponse(response)
                        }
                    }

    private fun getFirstVenuePhoto(venueId: String): Single<String?> =
            foursquareClient.getVenuePhotos(venueId)
                    .map { response ->
                        val photoItem = response.photos.photo.items.first()
                        "${photoItem.prefix}$SIZE${photoItem.suffix}"
                    }
                    .onErrorReturn { "" }

    private fun mapToSuccessVenuesResponse(response: SuccessRecommendationsResponse): Single<SuccessVenuesResponse>? {
        val venueList = response.venues.map { venue -> getFirstVenuePhoto(venue.id).map { mapVenue(venue, it) } }
        return Single.zip(venueList, mapVenues())
                .flatMap { list: List<Venue> -> Single.just(SuccessVenuesResponse(list)) }
    }

    private fun mapToSuccessRecommendationsResponse(response: VenueRecommendations.Response): SuccessRecommendationsResponse =
            SuccessRecommendationsResponse(response.responseItem.groups
                    .flatMap { group ->
                        group.items
                                .map { groupItem -> groupItem.venue }
                    })

    private fun mapVenue(apiVenue: VenueRecommendations.Venue, photoUrl: String?): Venue =
            Venue(apiVenue.name, photoUrl, apiVenue.location.address.joinToString(separator = "\n"))

    private fun mapVenues() = { venues: Array<Any> -> venues.map { venue -> venue as Venue }.toList() }

    companion object {
        const val SIZE = "500x300"
    }
}

sealed class RecommendationsResponse
data class EmptyRecommendationsResponse(val warning: String?) : RecommendationsResponse()
data class SuccessRecommendationsResponse(val venues: List<VenueRecommendations.Venue>) : RecommendationsResponse()

sealed class VenuesResponse
data class EmptyVenuesResponse(val warning: String?) : VenuesResponse()
data class SuccessVenuesResponse(val venues: List<Venue>) : VenuesResponse()