package com.brigitlyons.vlakbij.api

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object FoursquareClientProvider {

    @Volatile
    private var instance: FoursquareClient? = null
    @Volatile
    private var retrofitClient: Retrofit? = null

    fun getInstance(): FoursquareClient =
            instance ?: synchronized(this) {
                instance
                        ?: getRetrofitClient().create(FoursquareClient::class.java).also { instance = it }
            }

    private fun getRetrofitClient(): Retrofit =
            retrofitClient ?: synchronized(this) {
                retrofitClient ?: getRetroFit(getOkHttpClient()).also { retrofitClient = it }
            }

    private fun getOkHttpClient(): OkHttpClient =
            OkHttpClient.Builder()
                    .addInterceptor(getInterceptor())
                    .addNetworkInterceptor(StethoInterceptor())
                    .connectTimeout(NETWORK_TIMEOUT, NETWORK_TIMEOUT_UNIT)
                    .readTimeout(NETWORK_TIMEOUT, NETWORK_TIMEOUT_UNIT)
                    .build()

    private fun getInterceptor(): Interceptor =
            Interceptor { chain ->
                var request = chain.request()
                val url = request.url().newBuilder()
                        .addQueryParameter("client_id", API_CLIENT_ID)
                        .addQueryParameter("client_secret", API_CLIENT_SECRET)
                        .addQueryParameter("v", API_VERSION)
                        .build()
                request = request.newBuilder().url(url).build()
                chain.proceed(request)
            }

    private fun getRetroFit(okHttpClient: OkHttpClient): Retrofit =
            Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(Gson()))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClient)
                    .build()

    private const val NETWORK_TIMEOUT = 20L
    private val NETWORK_TIMEOUT_UNIT = TimeUnit.SECONDS
    private const val BASE_URL = "https://api.foursquare.com/v2/"
    private const val API_CLIENT_ID = "xxx"
    private const val API_CLIENT_SECRET = "yyy"
    private const val API_VERSION = "20180918"
}