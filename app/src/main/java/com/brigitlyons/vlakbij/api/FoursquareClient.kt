package com.brigitlyons.vlakbij.api

import com.brigitlyons.vlakbij.api.model.VenuePhotos
import com.brigitlyons.vlakbij.api.model.VenueRecommendations
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface FoursquareClient {

    @GET("venues/explore")
    fun getVenueRecommendations(@Query("ll") latitudeAndLongitude: String, @Query("query") query: String?): Single<VenueRecommendations.Response>

    @GET("venues/{id}/photos")
    fun getVenuePhotos(@Path("id") id: String): Single<VenuePhotos.Response>
}
