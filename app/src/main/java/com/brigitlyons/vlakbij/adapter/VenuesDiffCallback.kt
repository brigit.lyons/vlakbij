package com.brigitlyons.vlakbij.adapter

import android.support.v7.util.DiffUtil
import com.brigitlyons.vlakbij.ui.Venue

class VenuesDiffCallback : DiffUtil.ItemCallback<Venue>() {

    override fun areItemsTheSame(oldItem: Venue, newItem: Venue): Boolean =
            oldItem.name == newItem.name

    override fun areContentsTheSame(oldItem: Venue, newItem: Venue): Boolean =
            oldItem == newItem
}