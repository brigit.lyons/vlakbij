package com.brigitlyons.vlakbij.adapter

import android.databinding.DataBindingUtil
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.brigitlyons.vlakbij.R
import com.brigitlyons.vlakbij.databinding.VenueItemBinding
import com.brigitlyons.vlakbij.ui.Venue

class VenueListAdapter : ListAdapter<Venue, VenueListAdapter.ViewHolder>(VenuesDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VenueListAdapter.ViewHolder {
        return ViewHolder(
                DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        R.layout.venue_item, parent, false
                )
        )
    }

    override fun onBindViewHolder(holder: VenueListAdapter.ViewHolder, position: Int) {
        getItem(position).let { venue ->
            with(holder) {
                itemView.tag = venue
                bind(venue)
            }
        }
    }

    class ViewHolder(private val binding: VenueItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(venue: Venue) {
            with(binding) {
                setVenue(venue)
                executePendingBindings()
            }
        }
    }
}