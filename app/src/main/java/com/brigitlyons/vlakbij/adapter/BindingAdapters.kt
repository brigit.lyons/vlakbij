package com.brigitlyons.vlakbij.adapter

import android.databinding.BindingAdapter
import android.view.View
import android.widget.ImageView
import com.brigitlyons.vlakbij.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions

@BindingAdapter("goneUnless")
fun goneUnless(view: View, visible: Boolean?) {
    view.visibility = if (visible == null || visible) View.VISIBLE else View.GONE
}

@BindingAdapter("goneIf")
fun goneIf(view: View, visible: Boolean?) {
    view.visibility = if (visible == null || !visible) View.VISIBLE else View.GONE
}

@BindingAdapter("imageFromUrl")
fun bindImageFromUrl(view: ImageView, imageUrl: String?) {
    if (!imageUrl.isNullOrEmpty()) {
        Glide.with(view.context)
                .load(imageUrl)
                .apply(RequestOptions().fallback(R.drawable.venue_image_placeholder)
                        .placeholder(R.drawable.venue_image_placeholder)
                        .centerCrop())
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(view)
    } else {
        Glide.with(view.context)
                .load(R.drawable.venue_image_placeholder)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(view)
    }
}
