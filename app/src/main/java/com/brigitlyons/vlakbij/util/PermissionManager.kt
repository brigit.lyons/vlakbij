package com.brigitlyons.vlakbij.util

import android.app.Activity
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat


object PermissionManager {

    fun getState(activity: Activity, permission: String): State {
        return when (isPermissionGranted(activity, permission)) {
            PackageManager.PERMISSION_GRANTED -> State.GRANTED
            else ->
                when (canRequestPermission(activity, permission)) {
                    true -> State.REQUEST
                    else -> State.DENIED
                }
        }
    }

    private fun isPermissionGranted(activity: Activity, permission: String) =
            ActivityCompat.checkSelfPermission(activity, permission)

    private fun canRequestPermission(activity: Activity, permission: String) =
            ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)

    enum class State {
        GRANTED, REQUEST, DENIED
    }
}