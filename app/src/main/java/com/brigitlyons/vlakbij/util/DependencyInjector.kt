package com.brigitlyons.vlakbij.util

import com.brigitlyons.vlakbij.api.FoursquareClient
import com.brigitlyons.vlakbij.api.FoursquareClientProvider
import com.brigitlyons.vlakbij.api.FoursquareRepository
import com.brigitlyons.vlakbij.view.OptionsFragmentParams
import com.brigitlyons.vlakbij.view.VenuesFragmentParams
import com.brigitlyons.vlakbij.viewmodel.*

object DependencyInjector {
    fun provideVenuesViewModelFactory(venuesFragmentParams: VenuesFragmentParams, view: VenuesViewModel.View): VenuesViewModelFactory {
        val repository = getFoursquareRepository()
        return VenuesViewModelFactory(repository, venuesFragmentParams, view)
    }

    fun providePermissionsViewModelFactory(view: PermissionsViewModel.View): PermissionsViewModelFactory {
        return PermissionsViewModelFactory(view)
    }

    fun provideOptionsViewModelFactory(optionsFragmentParams: OptionsFragmentParams, view: OptionsViewModel.View): OptionsViewModelFactory {
        return OptionsViewModelFactory(optionsFragmentParams, view)
    }

    private fun getFoursquareRepository(): FoursquareRepository = FoursquareRepository(getFoursquareClient())

    private fun getFoursquareClient(): FoursquareClient = FoursquareClientProvider.getInstance()
}