package com.brigitlyons.vlakbij.ui

data class Venue(val name: String, val photoUrl: String?, val address: String?)