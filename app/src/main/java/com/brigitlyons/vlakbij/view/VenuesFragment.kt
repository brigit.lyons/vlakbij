package com.brigitlyons.vlakbij.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.location.Location
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.brigitlyons.vlakbij.R
import com.brigitlyons.vlakbij.adapter.VenueListAdapter
import com.brigitlyons.vlakbij.databinding.VenuesFragmentBinding
import com.brigitlyons.vlakbij.util.DependencyInjector
import com.brigitlyons.vlakbij.viewmodel.VenuesViewModel


class VenuesFragment : Fragment(), VenuesViewModel.View {

    private lateinit var viewModel: VenuesViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Obtain the ViewModel component.
        val factory = DependencyInjector.provideVenuesViewModelFactory(VenuesFragmentParams.fromBundle(requireNotNull(arguments)), this)
        viewModel = ViewModelProviders.of(this, factory).get(VenuesViewModel::class.java)

        // Inflate view and obtain an instance of the binding class.
        val binding = DataBindingUtil.inflate<VenuesFragmentBinding>(inflater, R.layout.venues_fragment, container, false)

        // set up the adapter
        val adapter = VenueListAdapter()
        binding.venuesList.adapter = adapter

        // Assign the component to a property in the binding class.
        binding.setLifecycleOwner(this)
        binding.viewmodel = viewModel
        subscribeUi(adapter)

        return binding.root
    }

    private fun subscribeUi(adapter: VenueListAdapter) {
        viewModel.getVenues().observe(viewLifecycleOwner, Observer { venues ->
            venues?.let { adapter.submitList(it) }
        })
    }

    override fun onDestroy() {
        viewModel.onDestroy()
        super.onDestroy()
    }

    override fun goBack() {
        fragmentManager?.popBackStackImmediate()
    }

    override fun errorHeader(): String = resources.getString(R.string.error_header)

    override fun errorSubText(): String = resources.getString(R.string.error_results_subtext)

    override fun errorButtonText(): String = resources.getString(R.string.error_results_button)

    override fun emptyHeader(): String = resources.getString(R.string.empty_results_header)

    override fun emptyButtonText(): String = resources.getString(R.string.empty_results_button)
}

data class VenuesFragmentParams(val location: Location, val query: String?) {

    companion object {
        private const val LOCATION_KEY = "LOCATION_KEY"
        private const val QUERY_KEY = "QUERY_KEY"

        fun toBundle(location: Location, query: String?): Bundle =
                Bundle().apply {
                    putParcelable(LOCATION_KEY, location)
                    query?.let { putString(QUERY_KEY, it) }
                }

        fun fromBundle(bundle: Bundle): VenuesFragmentParams =
                VenuesFragmentParams(requireNotNull(bundle.getParcelable(LOCATION_KEY)), bundle.getString(QUERY_KEY))
    }
}
