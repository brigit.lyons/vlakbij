package com.brigitlyons.vlakbij.view

import android.Manifest
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation

import com.brigitlyons.vlakbij.R
import com.brigitlyons.vlakbij.databinding.PermissionsFragmentBinding
import com.brigitlyons.vlakbij.util.Constants
import com.brigitlyons.vlakbij.util.DependencyInjector
import com.brigitlyons.vlakbij.util.PermissionManager
import com.brigitlyons.vlakbij.viewmodel.PermissionsViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

class PermissionsFragment : Fragment(), PermissionsViewModel.View {

    private lateinit var viewModel: PermissionsViewModel
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
    }

    override fun onStart() {
        super.onStart()
        requestPermission()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val factory = DependencyInjector.providePermissionsViewModelFactory(this)
        viewModel = ViewModelProviders.of(this, factory).get(PermissionsViewModel::class.java)

        val binding = DataBindingUtil.inflate<PermissionsFragmentBinding>(inflater, R.layout.permissions_fragment, container, false)
        binding.setLifecycleOwner(this)
        binding.viewmodel = viewModel

        return binding.root
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            Constants.REQUEST_LOCATION_PERMISSION_CODE -> viewModel.setPermissionState(getLocationPermissionState())
            else -> onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun requestPermission() {
        requestPermissions(arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), Constants.REQUEST_LOCATION_PERMISSION_CODE)
    }

    override fun openAppSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", requireActivity().packageName, null)
        intent.data = uri
        startActivity(intent)
    }

    @SuppressWarnings("MissingPermission")
    override fun getLocation() {
        if (getLocationPermissionState() == PermissionManager.State.GRANTED) {
            fusedLocationClient.lastLocation
                    .addOnSuccessListener { location ->
                        location?.let {
                            Navigation.findNavController(requireActivity(), R.id.permissions).navigate(R.id.action_permissionsFragment_to_optionsFragment, OptionsFragmentParams.toBundle(location))
                            viewModel.setLocation(it)
                        }
                    }
                    .addOnFailureListener { viewModel.setErrorState() }
                    .addOnCanceledListener { viewModel.setErrorState() }
        }
    }

    override fun getErrorHeader(): String = resources.getString(R.string.error_header)

    override fun getErrorSubText(): String = resources.getString(R.string.permissions_error_subtext)

    override fun getPermissionsHeader(): String = resources.getString(R.string.request_permissions_header)

    override fun getPermissionsSubText(): String = resources.getString(R.string.request_permissions_subtext)

    override fun getPermissionsRequestButtonText(): String = resources.getString(R.string.request_permissions_request_button)

    override fun getPermissionsSettingsButtonText(): String = resources.getString(R.string.request_permissions_setting_button)

    private fun getLocationPermissionState() =
            PermissionManager.getState(requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)

}
