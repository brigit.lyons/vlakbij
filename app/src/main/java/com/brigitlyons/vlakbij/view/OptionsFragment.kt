package com.brigitlyons.vlakbij.view

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.databinding.DataBindingUtil
import android.location.Location
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.brigitlyons.vlakbij.R
import com.brigitlyons.vlakbij.databinding.OptionsFragmentBinding
import com.brigitlyons.vlakbij.util.DependencyInjector
import com.brigitlyons.vlakbij.viewmodel.OptionsViewModel
import android.view.inputmethod.InputMethodManager


class OptionsFragment : Fragment(), OptionsViewModel.View {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val factory = DependencyInjector.provideOptionsViewModelFactory(OptionsFragmentParams.fromBundle(requireNotNull(arguments)), this)
        val viewModel = ViewModelProviders.of(this, factory).get(OptionsViewModel::class.java)

        val binding = DataBindingUtil.inflate<OptionsFragmentBinding>(inflater, R.layout.options_fragment, container, false)
        binding.viewModel = viewModel
        return binding.root
    }

    override fun closeKeyboard() {
        requireActivity().currentFocus.let { view ->
            val inputManager = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(view?.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        }
    }
}

data class OptionsFragmentParams(val location: Location) {

    companion object {
        private const val LOCATION = "LOCATION"

        fun toBundle(location: Location): Bundle = Bundle().apply { putParcelable(LOCATION, location) }

        fun fromBundle(bundle: Bundle): OptionsFragmentParams =
                OptionsFragmentParams(requireNotNull(bundle.getParcelable(LOCATION)))
    }
}
