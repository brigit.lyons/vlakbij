package com.brigitlyons.vlakbij.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.brigitlyons.vlakbij.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
