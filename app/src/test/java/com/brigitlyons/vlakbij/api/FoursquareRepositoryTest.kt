package com.brigitlyons.vlakbij.api

import com.brigitlyons.vlakbij.api.model.VenuePhotos
import com.brigitlyons.vlakbij.api.model.VenueRecommendations
import com.brigitlyons.vlakbij.util.VenueFixtures
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class FoursquareRepositoryTest {
    private val latitude = 1.2
    private val longitude = 2.1
    private val latitudeAndLongitude = "$latitude,$longitude"
    private val query = "tacos"

    private lateinit var repository: FoursquareRepository
    @Mock
    private lateinit var client: FoursquareClient

    @Before
    fun setup() {
        repository = FoursquareRepository(client)
    }

    @Test
    fun `it returns venues without query`() {
        initializeRecommendationResponse(Single.just(VenueFixtures.venueRecommendationResponse), query = null)
        initializePhotosResponse(Single.just(VenueFixtures.venuePhotosResponse))

        val expectedResult = VenueFixtures.successVenuesResponse

        repository.getVenueRecommendations(latitude, longitude, null).test().assertValue(expectedResult)

        verify(client).getVenueRecommendations(latitudeAndLongitude, null)
        verify(client).getVenuePhotos(VenueFixtures.venueId)
    }

    @Test
    fun `it returns venues for a query`() {
        initializeRecommendationResponse(Single.just(VenueFixtures.venueRecommendationResponse))
        initializePhotosResponse(Single.just(VenueFixtures.venuePhotosResponse))

        val expectedResult = VenueFixtures.successVenuesResponse

        repository.getVenueRecommendations(latitude, longitude, query).test().assertValue(expectedResult)

        verify(client).getVenueRecommendations(latitudeAndLongitude, query)
        verify(client).getVenuePhotos(VenueFixtures.venueId)
    }

    @Test
    fun `it returns venues without photos when no photos found`() {
        initializeRecommendationResponse(Single.just(VenueFixtures.venueRecommendationResponse))
        initializePhotosResponse(Single.just(VenueFixtures.venuePhotosResponseWithoutPhotos))

        val expectedResult = SuccessVenuesResponse(VenueFixtures.venuesWithoutPhotos)

        repository.getVenueRecommendations(latitude, longitude, query).test().assertValue(expectedResult)

        verify(client).getVenueRecommendations(latitudeAndLongitude, query)
        verify(client).getVenuePhotos(VenueFixtures.venueId)
    }

    @Test
    fun `it returns venues without photos when photo request fails`() {
        initializeRecommendationResponse(Single.just(VenueFixtures.venueRecommendationResponse))
        initializePhotosResponse(Single.error(Throwable("exception!")))

        val expectedResult = SuccessVenuesResponse(VenueFixtures.venuesWithoutPhotos)

        repository.getVenueRecommendations(latitude, longitude, query).test().assertValue(expectedResult)

        verify(client).getVenueRecommendations(latitudeAndLongitude, query)
        verify(client).getVenuePhotos(VenueFixtures.venueId)
    }

    @Test
    fun `it returns empty response when client returns warning and zero results`() {
        initializeRecommendationResponse(Single.just(VenueFixtures.venueRecommendationResponseWithWarningAndNoResults))

        val expectedResult = EmptyVenuesResponse(VenueFixtures.warning.text)

        repository.getVenueRecommendations(latitude, longitude, query).test().assertValue(expectedResult)

        verify(client).getVenueRecommendations(latitudeAndLongitude, query)
        verify(client, never()).getVenuePhotos(any())
    }

    @Test
    fun `it returns success response with venues when client returns warning and results`() {
        initializeRecommendationResponse(Single.just(VenueFixtures.venueRecommendationResponseWithWarningAndResults))
        initializePhotosResponse(Single.just(VenueFixtures.venuePhotosResponse))

        val expectedResult = VenueFixtures.successVenuesResponse

        repository.getVenueRecommendations(latitude, longitude, query).test().assertValue(expectedResult)

        verify(client).getVenueRecommendations(latitudeAndLongitude, query)
        verify(client).getVenuePhotos(VenueFixtures.venueId)
    }

    @Test
    fun `it returns empty response when client returns no warning or results`() {
        initializeRecommendationResponse(Single.just(VenueFixtures.venueRecommendationResponseWithoutWarningOrResults))

        val expectedResult = EmptyVenuesResponse(null)

        repository.getVenueRecommendations(latitude, longitude, query).test().assertValue(expectedResult)

        verify(client).getVenueRecommendations(latitudeAndLongitude, query)
        verify(client, never()).getVenuePhotos(any())
    }

    @Test
    fun `it returns error when client recommendation request fails`() {
        val exception = Throwable("exception!")
        initializeRecommendationResponse(Single.error(exception))

        repository.getVenueRecommendations(latitude, longitude, query).test().assertError(exception)

        verify(client).getVenueRecommendations(latitudeAndLongitude, query)
        verify(client, never()).getVenuePhotos(any())
    }

    private fun initializeRecommendationResponse(response: Single<VenueRecommendations.Response>, query: String? = this.query) {
        whenever(client.getVenueRecommendations(latitudeAndLongitude, query)).thenReturn(response)
    }

    private fun initializePhotosResponse(response: Single<VenuePhotos.Response>) {
        whenever(client.getVenuePhotos(VenueFixtures.venueId)).thenReturn(response)
    }

}