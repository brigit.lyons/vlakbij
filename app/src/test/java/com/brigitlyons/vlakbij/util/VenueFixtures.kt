package com.brigitlyons.vlakbij.util

import com.brigitlyons.vlakbij.api.FoursquareRepository
import com.brigitlyons.vlakbij.api.SuccessVenuesResponse
import com.brigitlyons.vlakbij.api.model.VenuePhotos
import com.brigitlyons.vlakbij.api.model.VenueRecommendations
import com.brigitlyons.vlakbij.ui.Venue

object VenueFixtures {
    const val venueId = "tacoplace123"

    val venues = listOf(Venue("The Best Taco Place", "http://xyz/${FoursquareRepository.SIZE}picture1.jpg", "123 Fake Street\nBoston, MA\nUSA"))

    val venuesWithoutPhotos = listOf(Venue("The Best Taco Place", "", "123 Fake Street\nBoston, MA\nUSA"))

    val warning = VenueRecommendations.Warning("Could not find any venues")

    val successVenuesResponse = SuccessVenuesResponse(venues)

    private val venueRecommendation = VenueRecommendations.Venue(
            "The Best Taco Place",
            venueId,
            VenueRecommendations.Location(listOf("123 Fake Street", "Boston, MA", "USA"))
    )

    val venueRecommendationResponse = createVenueRecommendationsResponse(
            listOf(VenueRecommendations.Group(
                    listOf(VenueRecommendations.GroupItem(venueRecommendation)))),
            null,
            1)

    val venueRecommendationResponseWithWarningAndResults = createVenueRecommendationsResponse(
            listOf(VenueRecommendations.Group(
                    listOf(VenueRecommendations.GroupItem(venueRecommendation)))),
            warning,
            1)

    val venueRecommendationResponseWithWarningAndNoResults = createVenueRecommendationsResponse(emptyList(), warning, 0)

    val venueRecommendationResponseWithoutWarningOrResults = createVenueRecommendationsResponse(emptyList(), null, 0)

    val venuePhotosResponse = createVenuePhotosResponse(
            listOf(VenuePhotos.PhotoItem("http://xyz/", "picture1.jpg"),
                    VenuePhotos.PhotoItem("http://xyz/", "picture2.jpg")
            )
    )

    val venuePhotosResponseWithoutPhotos = createVenuePhotosResponse(emptyList())

    private fun createVenueRecommendationsResponse(
            venues: List<VenueRecommendations.Group>,
            warning: VenueRecommendations.Warning?,
            totalResults: Int
    ): VenueRecommendations.Response =
            VenueRecommendations.Response(
                    VenueRecommendations.ResponseItem(
                            venues,
                            warning,
                            totalResults)
            )

    private fun createVenuePhotosResponse(photoItems: List<VenuePhotos.PhotoItem>): VenuePhotos.Response =
            VenuePhotos.Response(
                    VenuePhotos.Photos(
                            VenuePhotos.Photo(
                                    photoItems
                            )
                    )
            )
}