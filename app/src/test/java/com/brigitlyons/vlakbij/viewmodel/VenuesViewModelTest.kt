package com.brigitlyons.vlakbij.viewmodel

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.location.Location
import com.brigitlyons.vlakbij.api.EmptyVenuesResponse
import com.brigitlyons.vlakbij.api.FoursquareRepository
import com.brigitlyons.vlakbij.api.SuccessVenuesResponse
import com.brigitlyons.vlakbij.api.VenuesResponse
import com.brigitlyons.vlakbij.util.VenueFixtures
import com.brigitlyons.vlakbij.util.getValue
import com.brigitlyons.vlakbij.view.VenuesFragmentParams
import com.google.common.truth.Truth.assertThat
import com.nhaarman.mockitokotlin2.*
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.SingleSubject
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyDouble
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class VenuesViewModelTest {
    private val latitude = 1.2
    private val longitude = 2.1
    private val query = "tacos"
    private val view = TestView()

    private lateinit var viewModel: VenuesViewModel
    private lateinit var subject: SingleSubject<VenuesResponse>
    private lateinit var venuesFragmentParams: VenuesFragmentParams

    @Mock
    private lateinit var foursquareRepository: FoursquareRepository
    @Mock
    private lateinit var location: Location


    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        subject = SingleSubject.create()
        initialize(query)

        viewModel = VenuesViewModel(foursquareRepository, venuesFragmentParams, view, Schedulers.trampoline(), Schedulers.trampoline())
    }

    @Test
    fun `initialize the venues`() {
        assertThat(getVenues()).isNull()

        subject.onSuccess(VenueFixtures.successVenuesResponse)

        assertThat(getVenues()).isEqualTo(VenueFixtures.venues)
    }

    @Test
    fun `do not initialize the venues if disposable is disposed before response is emitted`() {
        assertThat(getVenues()).isNull()

        viewModel.onDestroy()
        subject.onSuccess(VenueFixtures.successVenuesResponse)

        assertThat(getVenues()).isNull()
    }

    @Test
    fun `update progress when the response is received`() {
        assertThat(getInProgress()).isTrue()

        subject.onSuccess(VenueFixtures.successVenuesResponse)

        assertThat(getInProgress()).isFalse()
    }

    @Test
    fun `no errors when the response is successful`() {
        assertInitialState()

        subject.onSuccess(VenueFixtures.successVenuesResponse)

        assertSuccessState()
    }

    @Test
    fun `set error data when the response fails`() {
        assertInitialState()

        subject.onError(Throwable("exception!"))

        assertErrorState()
    }

    @Test
    fun `set empty data when the response is empty`() {
        assertInitialState()

        subject.onSuccess(EmptyVenuesResponse(VenueFixtures.warning.text))

        assertEmptyState()
    }

    @Test
    fun `set empty data when the response is empty without a warning`() {
        assertInitialState()

        subject.onSuccess(EmptyVenuesResponse(null))

        assertEmptyState(subText = null)
    }

    @Test
    fun `set empty data when the response has empty list of venues`() {
        assertInitialState()

        subject.onSuccess(SuccessVenuesResponse(emptyList()))

        assertEmptyState(subText = null)
    }

    @Test
    fun `use the param values to get the venues`() {
        assertParamValuesUsed(query)
        assertParamValuesUsed("")
        assertParamValuesUsed(null)
    }

    private fun assertParamValuesUsed(query: String?) {
        reset(foursquareRepository)

        initialize(query)

        verify(foursquareRepository).getVenueRecommendations(latitude, longitude, query)
    }

    private fun initialize(query: String?) {
        whenever(location.latitude).thenReturn(latitude)
        whenever(location.longitude).thenReturn(longitude)
        venuesFragmentParams = VenuesFragmentParams(location, query)

        whenever(foursquareRepository.getVenueRecommendations(anyDouble(), anyDouble(), anyOrNull())).thenReturn(subject)

        viewModel = VenuesViewModel(foursquareRepository, venuesFragmentParams, view, Schedulers.trampoline(), Schedulers.trampoline())
    }

    private fun assertInitialState() {
        assertThat(getInProgress()).isTrue()
        assertThat(getError()).isFalse()
        assertThat(getEmpty()).isFalse()
        assertThat(getErrorHeader()).isNull()
        assertThat(getErrorSubText()).isNull()
        assertThat(getErrorButtonText()).isNull()
    }

    private fun assertSuccessState() {
        assertThat(getError()).isFalse()
        assertThat(getEmpty()).isFalse()
        assertThat(getErrorHeader()).isNull()
        assertThat(getErrorSubText()).isNull()
        assertThat(getErrorButtonText()).isNull()
        assertThat(getInProgress()).isFalse()
    }


    private fun assertErrorState() {
        assertThat(getError()).isTrue()
        assertThat(getEmpty()).isFalse()
        assertThat(getErrorHeader()).isEqualTo(view.errorHeader())
        assertThat(getErrorSubText()).isEqualTo(view.errorSubText())
        assertThat(getErrorButtonText()).isEqualTo(view.errorButtonText())
        assertThat(getInProgress()).isFalse()
    }

    private fun assertEmptyState(subText: String? = VenueFixtures.warning.text) {
        assertThat(getError()).isFalse()
        assertThat(getEmpty()).isTrue()
        assertThat(getErrorHeader()).isEqualTo(view.emptyHeader())
        assertThat(getErrorSubText()).isEqualTo(subText)
        assertThat(getErrorButtonText()).isEqualTo(view.emptyButtonText())
        assertThat(getInProgress()).isFalse()
    }

    private fun getVenues() = getValue(viewModel.getVenues())
    private fun getInProgress() = getValue(viewModel.getInProgress())
    private fun getError() = getValue(viewModel.getError())
    private fun getEmpty() = getValue(viewModel.getEmpty())
    private fun getErrorHeader() = getValue(viewModel.getErrorHeader())
    private fun getErrorSubText() = getValue(viewModel.getErrorSubText())
    private fun getErrorButtonText() = getValue(viewModel.getErrorButtonText())

    private class TestView : VenuesViewModel.View {
        override fun goBack() {
            // no-op
        }

        override fun errorHeader() = "error header"
        override fun errorSubText() = "error subtext"
        override fun errorButtonText() = "error button"
        override fun emptyHeader() = "empty header"
        override fun emptyButtonText() = "empty button"
    }
}